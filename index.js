const express = require("express");
const database = require("./db/index");
const routes = require("./routes");
const { emitter } = require("./emitter")

var app = express();

var port = process.env.PORT || 3000;

app.use(express.json());
app.use((uselessRequest, uselessResponse, neededNext) => database.onConnection(uselessRequest, uselessResponse, neededNext));

routes(app);

app.listen(port, () => {
  emitter();
  console.log(`App listening at http://localhost:${port}`);
});

// Do not change this line
module.exports = { app };