const users = require('./users');
const health = require('./health');
const transactions = require('./transactions');
const events = require('./events');
const bets = require('./bets');
const stats = require('./stats');

module.exports = (app) => {
  app.use("/users", users);
  app.use("/health", health);
  app.use("/transactions", transactions);
  app.use("/events", events);
  app.use("/bets", bets);
  app.use("/stats", stats);
}