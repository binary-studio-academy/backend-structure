const express = require("express");

const { errorHandler } = require("../helpers");
const { adminTokenValidate } = require("../middleware");
const { createTransactionValid } = require("../middleware/transactions/transactions.middleware");
const { createTransaction } = require("../services/transactions.service");

const router = express.Router();

router.post("/", adminTokenValidate, createTransactionValid, async (req, res) => {
  try {
    const transaction = await createTransaction(req.body, res);
    res.send(transaction)
  } catch (err) {
    errorHandler(res, 500, "Internal Server Error", err);
    return;
  }
});

module.exports = router;
