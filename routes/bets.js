const express = require("express");
const { errorHandler } = require("../helpers");
const { betTokenValidate } = require("../middleware");
const { createBetsValid } = require("../middleware/bets/bets.middleware");
const { createBet } = require("../services/bets.serivce");

const router = express.Router();

router.post("/", betTokenValidate, createBetsValid, async (req, res) => {  
  try {
    let userId = req.tokenUserId;
    const bet = await createBet(userId, req.body, res);
    res.send(bet);
  } catch (err) {
    errorHandler(res, 500, "Internal Server Error")
    return;
  }
});

module.exports = router;
