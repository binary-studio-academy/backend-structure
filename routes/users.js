const express = require("express");

const database = require('../db/index');
const { errorHandler } = require("../helpers");
const { tokenValidate } = require("../middleware");
const { getUserValid, createUserValid, updateUserValid } = require("../middleware/user/user.middleware");
const { getUser, createUser, updateUser } = require("../services/user.service");

const router = express.Router();

router.get("/:id", getUserValid, async (req, res) => {
  try {
    const user = await getUser(req.params.id, res);
    res.send(user);
  } catch (err) {
    errorHandler(res, 500, "Internal Server Error", err)
    return;
  }
});

router.post("/", createUserValid, async (req, res) => {
  try {
    req.body.balance = 0;

    const user = await createUser(req.body, res);
    res.send(user);
  } catch (err) {
    errorHandler(res, 500, "Internal Server Error", err)
    return;
  }
});

router.put("/:id", tokenValidate, updateUserValid, async (req, res) => {
  try {
    const user = await updateUser(req.params.id, req.body, res);
    res.send(user);
  } catch (err) {
    errorHandler(res, 500, "Internal Server Error", err)
    return;
  }
});

module.exports = router;