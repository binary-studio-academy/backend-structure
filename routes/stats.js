const express = require("express");
const { stats } = require("../emitter");
const { errorHandler } = require("../helpers");
const { adminTokenValidate } = require("../middleware");

const router = express.Router();

router.get("/", adminTokenValidate, (req, res) => {
  try {
    res.send(stats);
  } catch (err) {
    errorHandler(res, 500, "Internal Server Error", err);
    return;
  }
});

module.exports = router;
