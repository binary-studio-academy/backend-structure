const express = require("express");
const { errorHandler } = require("../helpers");

const { adminTokenValidate } = require("../middleware");
const { createEventValid, updateEventValid } = require("../middleware/events/events.middleware");
const { createEvent, updateEvent } = require("../services/events.service");

const router = express.Router();
router.use(adminTokenValidate);

router.post("/", createEventValid, async (req, res) => {  
  try {
    const event = await createEvent(req.body, res);
    res.send(event);
  } catch (err) {
    errorHandler(res, 500, "Internal Server Error", err)
    return;
  }
});

router.put("/:id", updateEventValid, async (req, res) => {
  try {
    await updateEvent(req.params.id, req.body, res);
  } catch (err) {
    errorHandler(res, 500, "Internal Server Error", err)
    return;
  }
});

module.exports = router;
