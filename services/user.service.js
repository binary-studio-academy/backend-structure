const jwt = require("jsonwebtoken");

const UserRepository = require("../db/repositories/user.repository");
const { errorHandler } = require("../helpers");

const getUser = async (id, res = null) => {
  const user = await UserRepository.getUser(id);

  if (!user && res) {
    errorHandler(res, 404, { error: 'User not found'});
    return;
  }

  return user;
};

const createUser = async (payload, res) => {
  const data = await UserRepository.createUser(payload);
  
  if (!data.err) {
    const { created_at: createdAt, updated_at: updatedAt, ...rest } = data[0];
    const newUser = {
      ...rest,
      createdAt,
      updatedAt,
      accessToken: jwt.sign(
        { id: rest.id, type: rest.type },
        process.env.JWT_SECRET
      ),
    };

    return newUser;
  }

  errorHandler(res, 400, { error: data.err });
  return;
};

const updateUser = async (id, payload, res) => {
  const data = await UserRepository.updateUser(id, payload);
  
  if (!data.err) {
    return data;
  }

  errorHandler(res, 400, { error: data.err });
  return;
};

module.exports = { getUser, createUser, updateUser };
