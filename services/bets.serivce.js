const ee = require('events');

const BetsRepository = require('../db/repositories/bets.repository');
const EventsRepository = require('../db/repositories/events.repository');
const DBRepository = require("../db/repositories/index.repository");
const UserRepository = require('../db/repositories/user.repository');
const { errorHandler } = require("../helpers");

var statEmitter = new ee();

const createBet = async (userId, payload, res) => {
  payload.event_id = payload.eventId;
  payload.bet_amount = payload.betAmount;
  delete payload.eventId;
  delete payload.betAmount;
  payload.user_id = userId;

  const users = await DBRepository.getTable('user');

  if (users) {
    const user = users.find(u => u.id == userId);

    if (!user) {
      errorHandler(res, 400, { error: 'User does not exist' })
      return;
    }

    if (+user.balance < +payload.bet_amount) {
      errorHandler(res, 400, { error: 'Not enough balance' })
      return;
    }

    const event = await EventsRepository.getEvent(payload.event_id);

    if (!event) {
      errorHandler(res, 404, { error: 'Event not found' });
      return;
    }

    const odds = await EventsRepository.getOdds(event.odds_id);

    if (!odds) {
      errorHandler(res, 404, { error: 'Odds not found' });
      return;
    }

    let multiplier;
    switch (payload.prediction) {
      case 'w1':
        multiplier = odds.home_win;
        break;
      case 'w2':
        multiplier = odds.away_win;
        break;
      case 'x':
        multiplier = odds.draw;
        break;
    }

    const bet = await BetsRepository.createBet({ ...payload, multiplier, event_id: event.id });
    const currentBalance = user.balance - payload.bet_amount;
    await UserRepository.updateUser(userId, { balance: currentBalance })

    statEmitter.emit('newBet');
    ['bet_amount', 'event_id', 'away_team', 'home_team', 'odds_id', 'start_at', 'updated_at', 'created_at', 'user_id'].forEach(whatakey => {
      var index = whatakey.indexOf('_');
      var newKey = whatakey.replace('_', '');
      newKey = newKey.split('')
      newKey[index] = newKey[index].toUpperCase();
      newKey = newKey.join('');
      bet[newKey] = bet[whatakey];
      delete bet[whatakey];
    });

    return { ...bet, currentBalance };
  }

  errorHandler(res, 400, { error: 'Bad request' });
  return;
};

module.exports = { createBet };
