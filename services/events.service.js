const ee = require('events');

const EventsRepository = require("../db/repositories/events.repository");
const BetsRepository = require("../db/repositories/bets.repository");
const UserRepository = require("../db/repositories/user.repository");
const { errorHandler } = require("../helpers");

var statEmitter = new ee();

const createEvent = async (payload, res) => {
  payload.odds.home_win = payload.odds.homeWin;
  delete payload.odds.homeWin;
  payload.odds.away_win = payload.odds.awayWin;
  delete payload.odds.awayWin;

  const updateOdds = await EventsRepository.createOdds(payload.odds);

  if (updateOdds) {
    delete payload.odds;
    payload.away_team = payload.awayTeam;
    payload.home_team = payload.homeTeam;
    payload.start_at = payload.startAt;
    delete payload.awayTeam;
    delete payload.homeTeam;
    delete payload.startAt;

    const newEvent = await EventsRepository.createEvent({ ...payload, odds_id: updateOdds.id });

    if (newEvent) {
      statEmitter.emit('newEvent');

      ['bet_amount', 'event_id', 'away_team', 'home_team', 'odds_id', 'start_at', 'updated_at', 'created_at'].forEach(whatakey => {
        var index = whatakey.indexOf('_');
        var newKey = whatakey.replace('_', '');
        newKey = newKey.split('')
        newKey[index] = newKey[index].toUpperCase();
        newKey = newKey.join('');
        newEvent[newKey] = newEvent[whatakey];
        delete newEvent[whatakey];
      });

      ['home_win', 'away_win', 'created_at', 'updated_at'].forEach(whatakey => {
        var index = whatakey.indexOf('_');
        var newKey = whatakey.replace('_', '');
        newKey = newKey.split('')
        newKey[index] = newKey[index].toUpperCase();
        newKey = newKey.join('');
        updateOdds[newKey] = updateOdds[whatakey];
        delete updateOdds[whatakey];
      })

      delete newEvent["createdAt"];
      delete updateOdds["updateOdds"];
      return { ...newEvent, odds: updateOdds };
    }

    errorHandler(res, 500, { error: 'Internal Server Error' });
  }

  errorHandler(res, 400, { error: 'Bad request' });
  return;
};

const updateEvent = async (eventId, payload, res) => {
  const bets = await BetsRepository.getBet(eventId, null);

  if (bets) {
    let [w1, w2] = payload.score.split(":");
    let result;

    if (+w1 > +w2) {
      result = 'w1'
    } else if (+w2 > +w1) {
      result = 'w2';
    } else {
      result = 'x';
    }

    const event = await EventsRepository.updateEvent(eventId, { score: payload.score });

    if (event) {
      await Promise.all(bets.map((bet) => {
        if (bet.prediction == result) {
          BetsRepository.updateBet(bet.id, { win: true });
          const user = UserRepository.getUser(bet.user_id);

          UserRepository.updateUser(bet.user_id, { balance: user.balance + (bet.bet_amount * bet.multiplier) });
          return;
        } else if (bet.prediction != result) {
          BetsRepository.updateBet(bet.id, { win: false })
          return;
        }
      }));

      setTimeout(() => {
        ['bet_amount', 'event_id', 'away_team', 'home_team', 'odds_id', 'start_at', 'updated_at', 'created_at'].forEach(whatakey => {
          var index = whatakey.indexOf('_');
          var newKey = whatakey.replace('_', '');
          newKey = newKey.split('')
          newKey[index] = newKey[index].toUpperCase();
          newKey = newKey.join('');
          event[newKey] = event[whatakey];
          delete event[whatakey];
        });

        res.send(event);
      }, 1000)
      return;
    }
    
    errorHandler(res, 400, { error: 'Bad request' });
    return;
  }

  errorHandler(res, 400, { error: 'Bad request' });
  return;
};

module.exports = { createEvent, updateEvent };
