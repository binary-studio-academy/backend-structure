const jwt = require("jsonwebtoken");

const TransactionRepository = require("../db/repositories/transactions.repository");
const userRepository = require("../db/repositories/user.repository");
const { updateUser } = require("../db/repositories/user.repository");
const { errorHandler } = require("../helpers");

const createTransaction = async (payload, res) => {
  payload.card_number = payload.cardNumber;
  payload.user_id = payload.userId;
  delete payload.cardNumber;
  delete payload.userId;

  const user = await userRepository.getUser(payload.user_id);

  if (user) {
    const transaction = await TransactionRepository.createTransaction(payload);

    if (transaction) {
      const currentBalance = payload.amount + user.balance;
  
      if (user) {
        await updateUser(payload.user_id, { 'balance': currentBalance });

        ['user_id', 'card_number', 'created_at', 'updated_at'].forEach(whatakey => {
          var index = whatakey.indexOf('_');
          var newKey = whatakey.replace('_', '');

          newKey = newKey.split('')
          newKey[index] = newKey[index].toUpperCase();
          newKey = newKey.join('');
          transaction[newKey] = transaction[whatakey];

          delete transaction[whatakey];
        })
  
        return { ...transaction, currentBalance: currentBalance };
      }
  
      errorHandler(res, 400, { error: "Bad request" })
      return;
    }

    errorHandler(res, 400, { error: "Bad transaction" })
    return;
  }

  errorHandler(res, 400, { error: "User does not exist" })
  return;
};

module.exports = { createTransaction };
