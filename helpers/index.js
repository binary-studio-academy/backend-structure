const errorHandler = (res, statusCode, message, err = {}) => {
  console.error(err)
  res.status(statusCode).send(message);
}

module.exports = { errorHandler };