const transactionsSchema = require('./transactions.schema');

const createTransactionValid = (req, res, next) => {
  var schema = transactionsSchema.post();
  var isValidResult = schema.validate(req.body);

  if(isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  };

  next();
}

module.exports = { createTransactionValid };