const jwt = require("jsonwebtoken");

const { errorHandler } = require("../helpers");

const validate = (req, res, next, userValidate = false, custom = false, callback = null) => {
  let token = req.headers['authorization'];
  let tokenPayload;

  if(!token) {
    errorHandler(res, 401, { error: 'Not Authorized' });
    return;
  }

  token = token.replace('Bearer ', '');

  try {
    tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
  } catch (err) {
    errorHandler(res, 401, { error: 'Not Authorized' }, err);
    return;
  }
 
  if (custom) {
    if (callback(tokenPayload)) {
      errorHandler(res, 401, { error: "Not Authorized" });
      return;
    }
  } else if (userValidate) {
    if(req.params.id !== tokenPayload.id) {
      errorHandler(res, 401, { error: 'UserId mismatch' });
      return;
    }
  } else {
    req.tokenUserId = tokenPayload.id;
  }

  next();
}

const tokenValidate = (req, res, next) => {
  validate(req, res, next, true);
}

const adminTokenValidate = (req, res, next) => {
  validate(req, res, next, false, true, (tokenPayload) => tokenPayload.type != 'admin');
}

const betTokenValidate = (req, res, next) => {
  validate(req, res, next, false)
}

module.exports = { tokenValidate, adminTokenValidate, betTokenValidate };