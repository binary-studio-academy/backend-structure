const { errorHandler } = require('../../helpers');
const betsSchema = require('./bets.schema');

const createBetsValid = (req, res, next) => {
  var schema = betsSchema.post();
  var isValidResult = schema.validate(req.body);

  if(isValidResult.error) {
    errorHandler(res, 400, { error: isValidResult.error.details[0].message })
    return;
  };

  next();
}

module.exports = { createBetsValid };