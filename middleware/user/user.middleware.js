const userSchema = require('./user.schema');

const getUserValid = (req, res, next) => {
  const schema = userSchema.get();
  const isValidResult = schema.validate(req.params);

  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  };

  next();
}

const createUserValid = (req, res, next) => {
  const schema = userSchema.post();
  const isValidResult = schema.validate(req.body);
  
  if(isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  };

  next();
}

const updateUserValid = (req, res, next) => {
  const schema = userSchema.update();
  const isValidResult = schema.validate(req.body);

  if (isValidResult.error) {
    res.status(400).send({ error: isValidResult.error.details[0].message });
    return;
  };

  next();
}

module.exports = { getUserValid, createUserValid, updateUserValid };