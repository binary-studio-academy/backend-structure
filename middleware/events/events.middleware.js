const { errorHandler } = require('../../helpers');
const eventsSchema = require('./events.schema');

const validate = (schema, body, res) => {
  var isValidResult = schema.validate(body);

  if(isValidResult.error) {
    errorHandler(res, 400, { error: isValidResult.error.details[0].message })
    return false;
  };

  return true;
}

const createEventValid = (req, res, next) => {
  var schema = eventsSchema.post();
  const isValid = validate(schema, req.body, res);

  if (isValid) {
    next();
  }
}

const updateEventValid = (req, res, next) => {
  const schema = eventsSchema.update();
  const isValid = validate(schema, req.body, res);

  if (isValid) {
    next();
  }
}

module.exports = { createEventValid, updateEventValid };