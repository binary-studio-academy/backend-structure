const knex = require("knex");
const dbConfig = require("../knexfile");

module.exports = {
  db: knex(dbConfig.development),
  onConnection: function(uselessRequest, uselessResponse, neededNext) {
    this.db.raw('select 1+1 as result').then(function () {
      neededNext();
    }).catch(() => {
      throw new Error('No db connection');
    });
  }
};
