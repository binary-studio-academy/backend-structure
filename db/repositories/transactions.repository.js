const database = require("../index");

const db = database.db;

class TransactionRepository {
  async createTransaction(payload) {
    try {
      const newTransaction = await db("transaction").insert(payload).returning("*").then(([result]) => result);

      return newTransaction;
    } catch {
      return false;
    }
  }
}

module.exports = new TransactionRepository();
