const database = require("../index");

const db = database.db;

class BetsRepository {
  async getBet(id, status) {
    const bet = await db('bet').where('event_id', id).andWhere('win', status).then(([bet]) => bet);

    return bet;
  }

  async createBet(payload) {
    const newBet = db("bet").insert(payload).returning("*").then(([bet]) => bet);

    return newBet;
  }

  async updateBet(id, payload={}) {
    const newBet = db('bet').where('id', id).update(payload).then(([bet]) => bet);

    return newBet;
  }
}

module.exports = new BetsRepository();
