const database = require("../index");

const db = database.db;

class DBRepository {
  async getTable(name) {
    const table = await db.select().table(name);

    return table;
  }
}

module.exports = new DBRepository();
