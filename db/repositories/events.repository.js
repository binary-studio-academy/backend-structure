const database = require("../index");

const db = database.db;

class EventsRepository {
  async getOdds(id) {
    const odds = db('odds').where('id', id).then(([odds]) => odds);

    return odds;
  }

  async createOdds(payload) {
    try {
      const newOdds = await db("odds").insert(payload).returning("*").then(([odds]) => odds);

      return newOdds;
    } catch {
      return false;
    }
  }

  async getEvent(id) {
    const event = db('event').where('id', id).then(([event]) => event);

    return event;
  }

  async createEvent(payload) {
    try {
      const newEvent = await db("event").insert(payload).returning("*").then(([event]) => event);

      return newEvent;
    } catch {
      return false;
    }
  }

  async updateEvent(eventId, payload) {
    try {
      const newEvent = await db('event').where('id', eventId).update(payload).returning('*').then(([event]) => event);

      return newEvent;
    } catch {
      return false;
    }
  }
}

module.exports = new EventsRepository();
