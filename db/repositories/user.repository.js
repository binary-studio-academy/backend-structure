const database = require("../index");

const db = database.db;

class UserRepository {
  async getUser(id) {
    const user = await db("user").where("id", id).returning("*").then(([result]) => result);

    return user;
  }

  async createUser(payload) {
    try {
      const newUser = await db("user").insert(payload).returning("*");

      return newUser;
    } catch (err) {
      return { err: err.detail };
    }
  }

  async updateUser(id, payload) {
    try {
      const updatedUser = await db("user").where("id", id).update(payload).returning("*").then(([result]) => ({ ...result }));

      return updatedUser;
    } catch (err) {
      
      return { err: err.detail };
    }
  }
}

module.exports = new UserRepository();
